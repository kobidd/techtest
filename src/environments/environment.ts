// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig: {
    apiKey: "AIzaSyCYpHP_e38RVHho40GepBA-XY7lAsQQq88",
    authDomain: "techtest1-bdf35.firebaseapp.com",
    databaseURL: "https://techtest1-bdf35.firebaseio.com",
    projectId: "techtest1-bdf35",
    storageBucket: "techtest1-bdf35.appspot.com",
    messagingSenderId: "137349828011",
    appId: "1:137349828011:web:26797626bf3f1cda1356ee",
    measurementId: "G-532TLM985V"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
