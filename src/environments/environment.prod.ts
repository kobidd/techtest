export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyCYpHP_e38RVHho40GepBA-XY7lAsQQq88",
    authDomain: "techtest1-bdf35.firebaseapp.com",
    databaseURL: "https://techtest1-bdf35.firebaseio.com",
    projectId: "techtest1-bdf35",
    storageBucket: "techtest1-bdf35.appspot.com",
    messagingSenderId: "137349828011",
    appId: "1:137349828011:web:26797626bf3f1cda1356ee",
    measurementId: "G-532TLM985V"
  }
};
