import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { TouchSequence } from 'selenium-webdriver';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  constructor(public authservice:AuthService) { }
  userDisplayEmail:any;

  ngOnInit() {
   this.authservice.user.subscribe(
     user=>{
       this.userDisplayEmail = user.email
     }
   )

    console.log(this.userDisplayEmail)

  }

}
