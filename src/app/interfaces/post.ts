export interface Post {
    userId:string,
    user:string,
    title:string,
    body:string,
    username?:string
}
