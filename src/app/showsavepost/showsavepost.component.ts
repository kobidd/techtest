import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-showsavepost',
  templateUrl: './showsavepost.component.html',
  styleUrls: ['./showsavepost.component.css']
})
export class ShowsavepostComponent implements OnInit {
userId:any
posts$:Observable<any>;
  constructor(private authservice:AuthService,
              public postsservice:PostsService,
              private router:Router) { }

  deletePost(id:string){
  this.postsservice.deletePost(this.userId,id);
  }
  addLikes(id:string,likes:any){
    likes++
    this.postsservice.updateLike(this.userId,id,likes)
    this.router.navigate(['savepost'])
  }
  ngOnInit() {
    this.authservice.user.subscribe(
      user=>{
        this.userId = user.uid
        console.log(this.userId)
         this.postsservice.getPosts(this.userId).subscribe(
          post=>{
            this.posts$ = post;
          }
        )
      }
    )

  }

}
