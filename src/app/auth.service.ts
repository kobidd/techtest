import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './interfaces/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user:Observable<User|null>
  constructor(public afAuth:AngularFireAuth,
              private router:Router) {
    this.user = this.afAuth.authState;
   }

   signUp(email:string,password:string){
     this.afAuth
     .auth
     .createUserWithEmailAndPassword(email,password)
     .then(
     user=>{
      this.router.navigate(['/welcome']);
     })
   }

   logOut(){
     this.afAuth.auth.signOut()
     .then(
     user=>{
      this.router.navigate(['/login'])}
     )
   }

   logIn(email:string,password:string){
     this.afAuth
     .auth
     .signInWithEmailAndPassword(email,password)
     .then(
     user=>{
      this.router.navigate(['/welcome'])})
   }

}
