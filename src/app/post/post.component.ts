import { AuthService } from './../auth.service';
import { PostsService } from './../posts.service';
import { Component, OnInit } from '@angular/core';
import { Post } from '../interfaces/post';
import { MatGridTileHeaderCssMatStyler } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
posts$:Post[];
show:boolean = false
postId:string
comments:any= []  
userId:string
saveCom:string
  constructor(public postsservice:PostsService,
              private authservice:AuthService,
              private router:Router) {
  }

  ngOnInit() {
    this.authservice.user.subscribe(
      user=>{
        this.userId = user.uid;
        console.log(this.userId)
      }
    )
    this.postsservice.getPostsData().subscribe(
  data1=>{
        console.log(data1)
        //console.log(data2)
        this.posts$  = data1
        //this.comments$ = data2
      }
    )
  }
  showcomments(postId:string){
    this.show = true;
    this.postId = postId;
    this.postsservice.getComment(postId).subscribe(p =>{
      this.comments = p
        console.log(JSON.stringify(this.comments));
      })
  }

  onSubmit(postId:string,title:string,body:string){
    this.postsservice.savePost(this.userId,postId,title,body)
    this.saveCom = "Saved for later viewing"
    //this.router.navigate(['post'])
  }

}
