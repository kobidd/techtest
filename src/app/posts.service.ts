import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Post } from './interfaces/post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';



@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private PostURL = "https://jsonplaceholder.typicode.com/posts/";
  private CommentURL = "https://jsonplaceholder.typicode.com/comments?postId=";

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  postCollection:AngularFirestoreCollection;

  savedPost:string
  saveIdpost:string

  constructor(private http:HttpClient,
              private db:AngularFirestore,
              private router:Router) { }
  getPostsData(){
    return this.http.get<Post[]>(`${this.PostURL}`)
  }
  getComment(postId:string){
    return this.http.get<Comment[]>(`${this.CommentURL}${postId}`)
  }
  savePost(userId:string,postId:string,title:string,body:string){
    const post = {postId:postId,title:title,body:body,like:0}
    this.userCollection.doc(userId).collection('posts').add(post)
    this.savedPost = "Saved for later viewing";
    this.saveIdpost= postId
    console.log(this.savedPost)
  }

  deletePost(userId:string,id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }

  updateLike(userId:string,id:string,likes:string){
    this.db.doc(`users/${userId}/posts/${id}`).update({
      like:likes}
    )
    }

  getPosts(userId:string):Observable<any>{
    this.postCollection = this.db.collection(`users/${userId}/posts`);
    return this.postCollection.snapshotChanges().pipe(
      map(collection=>collection.map(
        document=>{
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data
        }
      ))
    )
  }
 
}
